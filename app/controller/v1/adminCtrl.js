const jwt = require('jsonwebtoken');
const config = require('../../../config/config');

// models needed for this controller
const State = require('../../models/state');
const VAdmin = require('../../models/verificationadmin');

// Admin login route /v1/nbuild/admin/login
const loginAdmin = (req, res) => {
  console.log(req.body);

  VAdmin.findOne({
    username: req.body.username.toUpperCase(),
    password: req.body.password.toUpperCase(),
  })
    .select('password state')
    .exec((err, admin) => {
      if (err) {
        console.log('Error retrieveing Admin from database');
        return res.json({
          success: false,
          message: err,
        });
      }
      // no admin with that email was found
      if (!admin) {
        console.log('Admin user not found check username');
        return res.json({
          success: false,
          message: 'Authentication failed. Admin user not found.',
        });
      }
      // if user is found and password is right
      // create a token
      // eslint-disable-next-line
      const token = jwt.sign({ _id: admin._id, state: admin.state }, config.tokenSecret, {
        expiresIn: 1440 * 60, // expires in 24 hours //input is in seconds
      });

      // eslint-disable-next-line
      admin.last_login = new Date();
      admin.save((errrr) => {
        if (errrr) {
          console.log(errrr);
        }
      });
      const adminJSON = admin.toJSON();
      delete adminJSON.password; // remove password field before sending to client
      if (token && adminJSON) {
        return res.json({
          success: true,
          message: 'Auth successful!',
          token,
          admin: adminJSON,
        });
      }
      return res.json({
        success: false,
        message: 'Something went wrong contact admin',
      });
    });
};

// Admin creation route /v1/nbuild/admin/
const createAdmin = (req, res) => {
  const admin = new VAdmin();
  const { state } = req.body;
  if (state === undefined) {
    return res.json({
      success: false,
      message: 'Cannot create admin. No State Specified',
    });
  }
  const nstate = state.toUpperCase();
  console.log(nstate);
  State.findOne({ state: nstate })
    .select('state ')
    // eslint-disable-next-line
    .exec((err, state2) => {
      if (err) {
        console.log(err);
        return res.json({
          success: false,
          message:
            'Cannot create admin. check that the state is well spelt with unnecessary spacing',
        });
      }

      if (state2) {
        admin.username = req.body.username;
        admin.password = req.body.password;
        admin.state = req.body.state;
        admin.save((errr) => {
          if (errr) {
            console.log(errr);

            return res.json({
              success: false,
              message: 'Cannot create admin.',
            });
          }
          return res.json({
            success: true,
            message: 'Admin created successfully.',
          });
        });
      } else {
        return res.json({
          success: false,
          message: 'Cannot create admin. invalid state provided.',
        });
      }
    });
};

// Get all admins /v1/nbuild/admin/
const getAllAdmins = (req, res) => {
  let limit;
  if (req.query.limit === undefined) {
    limit = 0;
  } else {
    // eslint-disable-next-line
    limit = req.query.limit;
  }
  console.log(req.admin);
  VAdmin.find()
    .limit(limit)
    .exec((err, admins) => {
      if (err) {
        console.log(err);

        return res.json({
          success: false,
          message: 'Cannot get admins.',
        });
      }
      return res.json({
        success: true,
        data: admins,
      });
    });
};

// Get admin with ID admin_id /v1/nbuild/admin/:admin_id
const getParticularAdminById = (req, res) => {
  VAdmin.findById(req.params.admin_id, (err, admin) => {
    if (err) {
      console.log(err);

      return res.json({
        success: false,
        message: 'Cannot get admin.',
      });
    }
    /*
            return res.json({
                success: true,
                data: admin
            }); */
    return res.json(admin);
  });
};

// Update admin with ID admin_id /v1/nbuild/admin/:admin_id
const updateAdminById = (req, res) => {
  // eslint-disable-next-line
  VAdmin.findById(req.params.admin_id, (err, admin) => {
    if (err) {
      console.log(err);

      return res.json({
        success: false,
        message: 'Cannot update admin.',
      });
    }
    if (req.body.state === undefined) {
      return res.json({
        success: false,
        message: 'Cannot save admin. No state specified',
      });
    }

    State.findOne({ state })
      .select('state ')
      .exec((err, state) => {
        if (err) {
          console.log(err);

          return res.json({
            success: false,
            message: 'Cannot save admin.',
          });
        }

        if (state) {
          admin.username = req.body.username;
          if (!(req.body.password === undefined)) {
            admin.password = req.body.password;
          }
          admin.state = req.body.state;

          admin.save((err) => {
            if (err) {
              console.log(err);

              return res.json({
                success: false,
                message: 'Cannot save admin.',
              });
            }
            return res.json({
              success: true,
              message: 'Admin Updated successfully.',
            });
          });
        } else {
          return res.json({
            success: false,
            message: 'Cannot save admin. invalid state provided.',
          });
        }
      });
  });
};

// Delete admin with ID admin_id /v1/nbuild/admin/:admin_id
const deleteAdminById = (req, res) => {
  VAdmin.findOneAndRemove(req.params.admin_id, (err, admin) => {
    if (err) {
      console.log(err);

      return res.json({
        success: false,
        message: 'Cannot Delete admin.',
      });
    }
    // admin.
    return res.json({
      success: true,
      message: 'deleted successfully',
      data: admin,
    });
  });
};

// Get all states
const getAllStates = (req, res) => {
  State.find()
    .select('state state_id')
    .exec((err, states) => {
      if (err) {
        console.log(err);

        return res.json({
          success: false,
          message: 'Cannot get states.',
        });
      }
      return res.json({
        success: true,
        data: states,
      });
    });
};

module.exports = {
  loginAdmin,
  createAdmin,
  getAllAdmins,
  getParticularAdminById,
  updateAdminById,
  deleteAdminById,
  getAllStates,
};

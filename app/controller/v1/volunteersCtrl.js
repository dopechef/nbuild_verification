// models needed for this controller
const Selected = require('../../models/selected');
const State = require('../../models/state');
// Verify volunteer
// eslint-disable-next-line
const verifyVolunteer = (req, res) => {
  const { name, gender, age, address, ssce, id, altPhone, altEmail, rmk } = req.body;
  if (id === undefined) {
    return res.json({
      success: false,
      message: 'Please include the id of the volunteer',
    });
  }
  let status = true;
  if (gender === false) {
    status = false;
  }
  if (ssce === false) {
    status = false;
  }
  if (name === false) {
    status = false;
  }
  if (age === false) {
    status = false;
  }
  if (address === false) {
    status = false;
  }

  if (status === false) {
    Selected.updateOne(
      { _id: id, state_of_residence: req.admin.state.toUpperCase() },
      {
        $set: {
          status: 'disqualified',
          verificationdate: new Date(),
          verified_by: req.admin.id,
          rmk,
          alt_mobile_number: altPhone,
          alt_email_address: altEmail,
        },
      },
    ).exec((err, success) => {
      if (err) {
        return res.json({
          success: false,
          message: 'somethings wrong',
          data: err,
        });
      }
      return res.json({
        success: true,
        message: 'successfully verified and disqualified',
        data: success,
      });
    });
  } else {
    Selected.update(
      { _id: id, state_of_residence: req.admin.state.toUpperCase() },
      {
        $set: {
          verificationdate: new Date(),
          status: 'qualified',
          verified_by: req.admin.id,
          rmk,
          alt_mobile_number: altPhone,
          alt_email_address: altEmail,
        },
      },
    ).exec((err, success) => {
      if (err) {
        return res.json({
          success: false,
          message: 'somethings wrong',
          data: err,
        });
      }
      return res.json({
        success: true,
        message: 'successfully verified and qualified',
        data: success,
      });
    });
  }
};

// Get volunteer
const getVolunteers = (req, res) => {
  console.log('defined');
  Selected.findById(req.params.id, (err, result) => {
    if (err) {
      return res.json({
        success: false,
        message: 'Error',
        data: err,
      });
    }
    return res.json({
      success: true,
      message: 'Success',
      data: result,
    });
  });
};

// Get all volunteers
const getAllVolunteers = (req, res) => {
  let { limit, skip } = req.query;
  if (limit === undefined) {
    limit = 0;
  }
  if (skip === undefined) {
    skip = 0;
  }
  const state = req.admin.state.toUpperCase();
  Selected.find({ state_of_residence: state, program: 'N-Build' }).exec((err, result) => {
    if (err) {
      return res.json({
        success: false,
        message: 'Something Wrong Happened',
        data: err,
      });
    }
    return res.json({
      success: true,
      message: 'retreived records from Db',
      data: result,
    });
  });
};

// search volunteers
const searchVolunteer = async (req, res) => {
  if (req.body.input) {
    try {
      const data = await Selected.findOne({
        $or: [{ mobile_number: req.body.input }, { bvn: req.body.input }],
        state_of_residence: req.admin.state,
      });
      if (data) {
        res.json({ success: true, message: 'user details', data });
      } else {
        res.json({ success: false, message: 'user not found' });
      }
    } catch (e) {
      console.log(e);
      res.json({ success: false, data: e });
    }
  } else {
    res.json({
      success: false,
      message: 'Bad Request',
      data: null,
    });
  }
};

// unverify volunteer
// eslint-disable-next-line
const unverifyVolunteer = (req, res) => {
  const { id } = req.params;
  if (id === undefined) {
    return res.json({
      success: false,
      message: 'Please include the id of the volunteer',
    });
  }

  Selected.updateOne(
    { _id: id },
    {
      $set: {
        status: 'not_verified',
        verificationdate: new Date(),
        verified_by: req.admin.id,
      },
    },
  ).exec((err, success) => {
    if (err) {
      return res.json({
        success: false,
        message: 'somethings wrong',
        data: err,
      });
    }
    return res.json({
      success: true,
      message: 'status changed to not verified',
      data: success,
    });
  });
};

// volunteer stat helper function
const level1 = async (q, state) => {
  // compiled pending
  switch (q) {
    case 'compiled':
      return new Promise((resolve, reject) => {
        Selected.find({
          state_of_residence: state.toUpperCase(),
          $or: [{ status: 'disqualified' }, { status: 'qualified' }],
        })
          .count()
          .exec((err, result) => {
            if (err) {
              return reject(err);
            }
            return resolve(result);
          });
      });
    case 'pending':
      return new Promise((resolve, reject) => {
        Selected.find({ state_of_residence: state.toUpperCase(), status: 'not_verified' })
          .count()
          .exec((err, result) => {
            if (err) {
              return reject(err);
            }
            return resolve(result);
          });
      });
    case 'all':
      return new Promise((resolve, reject) => {
        console.log(state);
        Selected.find({ state_of_residence: state.toUpperCase() })
          .count()
          .exec((err, result) => {
            if (err) {
              return reject(err);
            }
            return resolve(result);
          });
      });
    default:
      return 'Invalid query parameter';
  }
};

// volunteer statistics
const volunteerStats = async (req, res) => {
  const { query } = req.query;
  const state = req.admin.state.toUpperCase();
  if (query) {
    return level1(query, state)
      .then(da => res.json({ success: true, data: da }))
      .catch(er => res.json({ success: false, data: er }));
  }
  return res.json({ success: false, message: 'Add necessary query params' });
};

const statsAggregate = (state) => {
  return new Promise(async (resolve, reject) => {
    try {
      // for all compiled
      const compiled = await Selected.aggregate([
        {
          $match: {
            state_of_residence: state.toUpperCase(),
            $or: [{ status: 'disqualified' }, { status: 'qualified' }],
          },
        },
        {
          $group: { _id: '$program', count: { $sum: 1 } },
        },
      ]);

      const qualified = await Selected.aggregate([
        {
          $match: {
            state_of_residence: state.toUpperCase(),
            status: 'qualified',
          },
        },
        {
          $group: { _id: '$program', count: { $sum: 1 } },
        },
      ]);

      const disqualified = await Selected.aggregate([
        {
          $match: {state_of_residence: state.toUpperCase(), status: 'disqualified'},
        },
        {
          $group: { _id: '$program', count: { $sum: 1 } },
        },
      ]);

      // for all not verified
      const notVerified = await Selected.aggregate([
        {
          $match: {
            state_of_residence: state.toUpperCase(),
            status: 'not_verified',
          },
        },
        {
          $group: { _id: '$program', count: { $sum: 1 } },
        },
      ]);

      // for all volunteers
      const all = await Selected.aggregate([
        {
          $match: { state_of_residence: state.toUpperCase() },
        },
        {
          $group: { _id: '$program', count: { $sum: 1 } },
        },
      ]);

      // result presentation
      const result = {
        state,
        compiled: compiled[0] ? compiled : 0,
        disqualified: disqualified[0] ? disqualified : 0,
        qualified: qualified[0] ? qualified : 0,
        not_verified: notVerified[0] ? notVerified : 0,
        total: all ? all[0] : 0,
      };
      resolve(result);
    } catch (error) {
      reject(error);
    }
  });
}
// eslint-disable-next-line
const allStats = async (req, res) => {
  const aggArray = [];
  const states = await State.find({});
  // eslint-disable-next-line
  for (let stat of states) {
    const { state } = stat;
    // eslint-disable-next-line
    const da = await statsAggregate(state);
    aggArray.push(da);
  }
  return res.json({
    success: true,
    data: aggArray,
  });
};
module.exports = {
  verifyVolunteer,
  unverifyVolunteer,
  getVolunteers,
  searchVolunteer,
  volunteerStats,
  getAllVolunteers,
  allStats,
};

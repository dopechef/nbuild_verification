const express = require('express');
const jwt = require('jsonwebtoken');
const AdminController = require('../../controller/v1/adminCtrl');
const VolunteerController = require('../../controller/v1/volunteersCtrl');
const config = require('../../../config/config');
const VAdmin = require('../../models/verificationadmin');

const api = express.Router();

// Admin login route
api.post('/vadmin/login', AdminController.loginAdmin);

// Create Admin route
api.post('/vadmin', AdminController.createAdmin);

// To get all states
api.get('/states', AdminController.getAllStates);

// To get all statistics
api.get('/vadmin/allstats', VolunteerController.allStats);

// Ensuring they produce the API token
// eslint-disable-next-line
api.use((req, res, next) => {
  if (req.headers.authorization === undefined) {
    return res.status(403).send({
      success: false,
      message: 'No token provided.',
    });
  }
  let token;
  try {
    token = req.headers.authorization;
    // console.log(req.headers['authorization']);
    // console.log(req.headers);
    // console.log(token);
    token = token.substr(7);
  } catch (err) {
    return res.status(403).send({
      success: false,
      message: 'Invalid or no authorization token provided.',
    });
  }

  // decode token
  if (token) {
    // verifies secret and checks exp
    // eslint-disable-next-line
    jwt.verify(token, config.tokenSecret, (err, decoded) => {
      if (err) {
        return res.status(403).send({
          success: false,
          message: 'Failed to authenticate token.',
        });
      }
      // eslint-disable-next-line
      VAdmin.findById(decoded._id, (errr, admin) => {
        if (errr) {
          console.log('nothing found');
          return res.json({
            success: false,
            message: 'Authentication failed. Admin user not found.',
          });
        }
        // if everything is good, save to request for use in other routes
        req.token = decoded;
        console.log(admin);
        req.admin = admin;
        next();
      });
    });
    // if there is no token
  } else {
    // if there is no token
    // return an HTTP response of 403 (access forbidden) and an error message
    return res.status(403).send({
      success: false,
      message: 'No token provided.',
    });
  }
});

// Get all admins with pagination by query
api.get('/vadmin', AdminController.getAllAdmins);

// Get admin with ID admin_id
api.get('/vadmin/:admin_id', AdminController.getParticularAdminById);

// Update admin with ID admin_id
api.put('/vadmin/:admin_id', AdminController.updateAdminById);

// Delete admin with ID admin_id
api.delete('/vadmin/:admin_id', AdminController.deleteAdminById);

// Search for a volunteer by bvn and number
api.post('/vadmin/volunteers/search', VolunteerController.searchVolunteer);

// Get volunteer by single id
api.get('/volunteers/:id', VolunteerController.getVolunteers);

// get all volunteers
api.get('/volunteers', VolunteerController.getAllVolunteers);

// verify volunteer
api.post('/vadmin/volunteers/verify', VolunteerController.verifyVolunteer);

// unverify volunteer
api.post('/vadmin/volunteers/unverify/:id', VolunteerController.unverifyVolunteer);

// volunteer statistics
api.get('/volunteerstats', VolunteerController.volunteerStats);

module.exports = api;

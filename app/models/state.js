const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');


const StateSchema = mongoose.Schema({

  state: { type: String, required: true },
  state_id: { type: String, required: true },
  devices_info: [
    {
      device_batch: { type: String, required: false },
      shipped_devices: { type: Number, required: false },
      notified_volunteers: { type: Number, required: false },
    },
  ],
});


// create the model for users and expose it to our app
module.exports = mongoose.model('state', StateSchema);
//module.exports = StateSchema;

// grab the packages that we need for the admin model
const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');


mongoose.Promise = require('bluebird');

// admin schema
const AdminSchema = new Schema({
  name: { fname: { type: String, required: true }, lname: { type: String, required: true } },
  email: { type: String, required: true, index: { unique: true } },
  phone: { type: String, required: true, index: { unique: true } },
  password: { type: String, select: true },
  date_created: { type: Date, default: Date.now },
  last_login: Date,
  twofactorauth: { type: String, enum: ['none', 'sms', 'email', 'mobiletoken'] },
  role: { type: Schema.Types.ObjectId, required: true },
  // permissions: [{type: String}]
});

// hash the password before the admin is saved
AdminSchema.pre('save', function hashPasword(next) {
  const admin = this;

  // hash the password only if the password has been changed or admin is new
  if (!admin.isModified('password')) return next();

  // generate the hash
  bcrypt.hash(admin.password, null, null, (err, hash) => {
    if (err) return next(err);

    // change the password to the hashed version
    admin.password = hash;
    next();
  });
});


// method to compare a given password with the database hash
AdminSchema.methods.comparePassword = function (password) {
  const admin = this;
  return bcrypt.compareSync(password, admin.password);
};

// return the model
// module.exports = mongoose.model('Admin', AdminSchema);


module.exports = mongoose.model('nbuildadmin', AdminSchema);


const mongoose = require('mongoose');

const { Schema } = mongoose;

mongoose.Promise = require('bluebird');

const SelectedSchema = mongoose.Schema({
  gender: { type: String, required: false },
  first_name: { type: String, required: false },
  middle_name: { type: String, required: false },
  last_name: { type: String, required: false },
  mobile_number: { index: true, type: String, required: true },
  alt_mobile_number: { type: String, required: false },
  email_address: { type: String, required: false },
  alt_email_address: { type: String, required: false },
  lga_of_residence: { type: String, required: false },
  state_of_residence: { type: String, required: false },
  address: { type: String, required: false },
  lga_of_origin: { type: String, required: false },
  state_of_origin: { type: String, required: false },
  bvn: { index: true, type: String, required: true },
  birth_day: { type: String, required: false },
  birth_month: { type: String, required: false },
  birth_year: { type: String, required: false },
  program: { type: String, required: false },
  status: { type: String, required: false, enum: ['not_verified', 'qualified', 'disqualified'] },
  verified_by: { type: Schema.Types.ObjectId, required: false },
  verification_date: { type: Date, required: false },
  rmk: { type: String, required: false },
});

// create the model for users and expose it to our app
module.exports = mongoose.model('selected', SelectedSchema);
// module.exports = SelectedSchema;

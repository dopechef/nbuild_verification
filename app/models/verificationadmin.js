// grab the packages that we need for the admin model
const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');


mongoose.Promise = require('bluebird');

// admin schema
const VerificationAdminSchema = new Schema({
  username: { type: String, required: true, index: { unique: true } },
  password: { type: String, select: false },
  state: { type: String, required: true },
  date_created: { type: Date, default: Date.now },
  last_login: Date,
  disabled: { type: Boolean, default: false },
});

// hash the password before the admin is saved
VerificationAdminSchema.pre('save', function (next) {
  const admin = this;

  // hash the password only if the password has been changed or admin is new
  if (!admin.isModified('password')) return next();

  // generate the hash
  bcrypt.hash(admin.password, null, null, (err, hash) => {
    if (err) return next(err);

    // change the password to the hashed version
    admin.password = hash;
    next();
  });
});


// method to compare a given password with the database hash
VerificationAdminSchema.methods.comparePassword = function (password) {
  const admin = this;
  return bcrypt.compareSync(password, admin.password);
};

// return the model
module.exports = mongoose.model('verificationadmin', VerificationAdminSchema);

// return the schema
//module.exports = VerificationAdminSchema;

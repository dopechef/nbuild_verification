const cluster = require('cluster');
const numWorkers = require('os').cpus().length;
const express = require('express');
const mws = require('./config/middlewares');

const app = express();
const routes = require('./app/index');
const dbConnection = require('./config/dbNbuild');
require('dotenv').config();
// use middlewares
app.use(mws);
process.env.PORT = 3000;

// connect to db
dbConnection();

// use route
// app.use(auth)
app.use('/v1/nbuild', routes.admin);

// connect to server custer
if (cluster.isMaster) {
  console.log(`Master cluster setting up ${numWorkers} workers...`);

  for (let i = 0; i < numWorkers;) {
    cluster.fork();
    i += 1;
  }

  cluster.on('online', (worker) => {
    console.log(`Worker ${worker.process.pid} is online`);
  });

  cluster.on('exit', (worker, code, signal) => {
    console.log(`Worker ${worker.process.pid} died with code: ${code}, and signal: ${signal}`);
    console.log('Starting a new worker');
    cluster.fork();
  });
} else {
  app.listen(process.env.PORT, () => {
    console.log(`app running on port: ${process.env.PORT}`);
  });
}
